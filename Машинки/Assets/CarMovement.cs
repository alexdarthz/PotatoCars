using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{
    public GameObject car;
    public float speed = 0.4f;
    public float carMoveX = 0.5f;
    void Start()
    {

    }
    void FixedUpdate()
    {
        movement();
    }
    public void movement()
    {
        if (Input.GetButton("Horizontal"))
        {
            Vector3 direction = car.transform.right * Input.GetAxis("Horizontal");
            car.transform.position = Vector3.MoveTowards(car.transform.position, car.transform.position + direction, Time.deltaTime * carMoveX);
            if (car.transform.position.x > 1.9f)
            {
                car.transform.position = Vector3.MoveTowards(car.transform.position, car.transform.position + direction, Time.deltaTime * -carMoveX);
            }
            else if (car.transform.position.x < -1.9f)
            {
                car.transform.position = Vector3.MoveTowards(car.transform.position, car.transform.position + direction, Time.deltaTime * -carMoveX);
            }
        }
    }
}
